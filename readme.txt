The Sprd_Assortment module is a useful extension for your shop to provide the customers with further product information like size charts, available colors, product pictures and descriptions.
Requirements

    Drupal 6
    sprd_api module
    Token module
    Nodewords module
    Page Title module

Installation

    If you have not already done so, get and install the Spreadshirt Articles module from the drupal project page.
    Place this module in your modules directory and enable it at admin/modules.

Project Information

    Maintenance status: Actively maintained
    Development status: not under development anymore as we plan to create a Drupal 7 version

Useful Modules

    Spreadshirt Articles

